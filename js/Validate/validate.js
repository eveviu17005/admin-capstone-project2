

export let validateEmpty = (input, spanError)=>{
    let span = document.getElementById(`${spanError}`)
   
    if(input.length == 0){
    
        
        span.classList.remove('err');
        span.classList.add('d-err')
        span.innerText = "Do not leave this field empty"
     
        return false
    }else{
        span.classList.remove('d-err')
        span.classList.add('err')
        return true
    }
}
export let validateString = (input, spanError)=>{
    let isValid = input*1
    let span = document.getElementById(`${spanError}`)
    const re = new RegExp('^[A-Z][a-z]*');
    let isString = re.test(isValid)
  
    if(!isString){
        span.classList.remove('err');
        span.classList.add('d-err')
        span.innerText = "Please enter a valid characters"
      
     
        return false
    }else{
        span.classList.remove('d-err')
        span.classList.add('err')
        return true
    }
}
   
export let checkURL =(url, spanError)=> {
  let isImg = url.match(/^http[^\?]*.(jpg|jpeg|gif|png|tiff|bmp)(\?(.*))?$/gmi) != null
  let span = document.getElementById(`${spanError}`)
  if(!isImg){
        span.classList.remove('err');
        span.classList.add('d-err')
        span.innerText = "Please copy link img address and place it here!"
  
     
        return false
  }else{
        span.classList.remove('d-err')
        span.classList.add('err')
        return true
  }
}

export let validateNum =(input, spanError)=>{
    let valInput = input*1
    let span = document.getElementById(`${spanError}`)
    let isNum = /^\d+$/.test(valInput);

    if(!isNum){
        span.classList.remove('err');
        span.classList.add('d-err')
        span.innerText = "Please enter a valid number"
       
     
        return false
    }else{
        span.classList.remove('d-err')
        span.classList.add('err')
        return true 
    }
}

