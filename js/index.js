import {
  deleteProduct,
  getProductById,
  getProductList,
  postProduct,
  putProduct,
} from "./API_Services/services.js";
import {
  getFormInfo,
  renderProductList,
  showProduct,
  validate,
} from "./Controller/controller.js";
import { validateEmpty } from "./Validate/validate.js";

let batLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
let tatLoading = () => {
  document.getElementById("loading").style.display = "none";
};
// let saveLocalStorage()=>{
//   let data = 
// }

let fectProductList = () => {
  batLoading();
  getProductList()
    .then((item) => {
      tatLoading();
      Swal.fire("Get product list success");
      renderProductList(item.data);
    })
    .catch((err) => {
      tatLoading();
      Swal.fire("Get product list fail");
      console.log("err: ", err);
    });
};
fectProductList();

let removeItem = (id) => {
  batLoading();
  deleteProduct(id)

    .then((res) => {
      tatLoading();
      Swal.fire("Remove success");
     fectProductList()
    })
    .catch((err) => {
      tatLoading();
      Swal.fire("Fail to remove");
      console.log("err: ", err);
    });
};

window.removeItem = removeItem;

let addProducts = () => {
  let isValid = true;
  batLoading();
  let product = getFormInfo();
  console.log('product: ', product);

  isValid = validate(product)
  if(isValid){
  postProduct(product)
    .then((item) => {
      tatLoading();
      Swal.fire("Add product success");
      fectProductList()
    })
    .catch((err) => {
      tatLoading();
      Swal.fire("Fail");
      console.log("err: ", err);
    });
  document.getElementById("inputForm").reset();
  }
};

window.addProducts = addProducts;

let showProductInfo = (id) => {
  batLoading();
  getProductById(id)
    .then((item) => {
      tatLoading();
      Swal.fire("Get info success");
      showProduct(item.data);
    })
    .catch((err) => {
      tatLoading();
      Swal.fire("Error");
      console.log("err: ", err);
    });
  document.getElementById("addBTN").disabled = true;
};

window.showProductInfo = showProductInfo;

var updateProduct = () => {
  batLoading();
  var product = getFormInfo();
  let isValid = true
  isValid = validate(product)
  console.log('product.id: ', product.id);
  if(isValid){
  putProduct(product.id, product)
    .then((item) => {
      tatLoading();
      Swal.fire("Complete update");
      setTimeout(fectProductList(), 10000);
    })
    .catch((err) => {
      tatLoading();
      Swal.fire("Error");
      console.log("err: ", err);
    });
  document.getElementById("inputForm").reset();
  document.getElementById("addBTN").disabled = false;
  }
}

window.updateProduct = updateProduct;
