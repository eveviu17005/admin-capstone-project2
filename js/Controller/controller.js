import { Product } from "../models/models.js";
import { checkURL, validateEmpty, validateNum, validateString } from "../Validate/validate.js";

export let renderProductList = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    contentHTML += `<tr class="white-space-no-wrap odd">
   
        <td class="">
          <div
            class="active-project-1 d-flex align-items-center mt-0"
          >
            <div class="h-avatar is-medium">
              <img
                class="avatar rounded"
                alt="user-icon"
                src="${item.img}"
              />
            </div>
            <div class="data-content text-left">
              <div>
                <span class="font-weight-bold"
                  >${item.name}</span
                >
              </div>
              <p class="m-0 mt-1">Screen: ${item.screen}; BackCamera: ${item.backCamera}; FrontCamera: ${item.frontCamera}</p>
            </div>
          </div>
        </td>
        <td class="sorting_1">${item.type}</td>
        <td class="text-right">${item.price}</td>

        <td>${item.desc}</td>
        <td>
          <div
            class="d-flex justify-content-end align-items-center"
          >
            <a
              style="cursor: pointer"
              data-toggle="modal"
              data-target="#myModal"
              onclick="showProductInfo(${item.id})"
              
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="text-secondary mx-4"
                width="20"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                ></path>
              </svg>
            </a>
            <a
              class="badge bg-danger"
              data-toggle="tooltip"
              data-placement="top"
              title=""
              data-original-title="Delete"
              href="#"
              onclick="removeItem(${item.id})"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                ></path>
              </svg>
            </a>
          </div>
        </td>
      </tr>`;
  });
  document.getElementById("tBody").innerHTML = contentHTML;
};

export let getFormInfo = () => {
  let id = document.getElementById("idSP").value.trim();
  let name = document.getElementById("TenSP").value.trim();
  let brand = document.getElementById("Brand").value.trim();
  let price = document.getElementById("GiaSP").value.trim();
  let screen = document.getElementById("Screen").value.trim();
  let backCamera = document.getElementById("backCamera").value.trim();
  let frontCamera = document.getElementById("frontCamera").value.trim();
  let img = document.getElementById("HinhSP").value.trim();
  let desc = document.getElementById("MoTa").value.trim();
  let product = new Product(
    id,
    name,
    brand,
    price,
    screen,
    `${backCamera}`,
    `${frontCamera}`,
    img,
    desc
  );
  return product;
};

export let showProduct = (product) => {
  document.getElementById("idSP").value = product.id;
  document.getElementById("TenSP").value = product.name;
  document.getElementById("Brand").value = product.type;
  document.getElementById("GiaSP").value = product.price;
  document.getElementById("Screen").value = product.screen;
  document.getElementById("backCamera").value = product.backCamera;
  document.getElementById("frontCamera").value = product.frontCamera;
  document.getElementById("HinhSP").value = product.img;
  document.getElementById("MoTa").value = product.desc;
};

export let validate = (item) =>{
let isValid = true;
isValid &=   validateEmpty(item.name, "nameError") && validateString(item.name, "nameError")
isValid &=  validateEmpty(item.type,"brandError") && validateString(item.type,"brandError")
isValid &=  validateEmpty(item.price, "priceError") && validateNum(item.price, "priceError")
isValid &=  validateEmpty(item.screen, "screenError")&& validateNum(item.screen, "screenError")
isValid &=  validateEmpty(item.backCamera, "backCamError") && validateNum(item.backCamera, "backCamError")
isValid &=  validateEmpty(item.frontCamera, "frontCamError") && validateNum(item.frontCamera, "frontCamError")
isValid &=  validateEmpty(item.img, "imgError") && validateString(item.img, "imgError") && checkURL(item.img, "imgError")
isValid &=  validateEmpty(item.desc, "descError")&& validateString(item.desc, "descError")
return isValid
}

